# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/pi/suchai-flight-software/apps/langmuir/src/system/cmdAPP.c" "/home/pi/suchai-flight-software/apps/langmuir/build/CMakeFiles/suchai-app.dir/src/system/cmdAPP.o"
  "/home/pi/suchai-flight-software/apps/langmuir/src/system/langmuirp.c" "/home/pi/suchai-flight-software/apps/langmuir/build/CMakeFiles/suchai-app.dir/src/system/langmuirp.o"
  "/home/pi/suchai-flight-software/apps/langmuir/src/system/main.c" "/home/pi/suchai-flight-software/apps/langmuir/build/CMakeFiles/suchai-app.dir/src/system/main.o"
  "/home/pi/suchai-flight-software/apps/langmuir/src/system/taskHousekeeping.c" "/home/pi/suchai-flight-software/apps/langmuir/build/CMakeFiles/suchai-app.dir/src/system/taskHousekeeping.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
