import serial
from time import sleep
from crccheck.crc import Crc16CcittFalse
 
import RPi.GPIO as GPIO 
import sys 

class PIP:
    def __init__(self, port="/dev/ttySC0", baudrate = 115200):
        self.ser = serial.Serial(port, baudrate)
        #self.data = -1
        #self.b_id= -1
        #self.hk_id= -1
        #self.hk= -1
        #self.hgc= -1
        #self.lgc= -1
        #self.crc= -1
        
    #def example_measure(self, data):
    #    self.data = data
        
    def measure(self):        
        self.ser.write(bytes('U', 'utf-8'))
        received_data = self.ser.read() 
        sleep(0.01)
        data_left = self.ser.inWaiting()
        received_data += self.ser.read(data_left)
        #print(received_data)
        return received_data

    def parse(self, data):
        #bit 1
        b_id = (0xE0 & data[0]) >>5
        hk_id = (0x1C & data[0]) >>2
        h_hk = 0x03 & data[0]
        #print(bin(self.data[0]))
        #print(bin(number))
        #print(bin(hk_id)) 
        #print(bin(h_hk))
        
        #bit 2
        
        #print(bin(n[1]))
        hk = int.from_bytes([h_hk, data[1]],'big',signed=False)
        #print(int.from_bytes([h_hk,self.data[1]],'big',signed=False))
        #bit 3
        
        #bit 4
        hgc = int.from_bytes([data[2], data[3]],'big', signed=False)
        #print(int.from_bytes([n[2],n[3]],'big',signed=False))
    
        #bit 5
        
        #bit 6
        lgc = int.from_bytes([data[4], data[5]],'big',signed=False)
        #print(int.from_bytes([n[4],n[5]],'big',signed=False))
        
        #bit 7

        #bit 8
        crc = int.from_bytes([data[6], data[7]],'big',signed=False)
        #print(int.from_bytes([n[6],n[7]],'big',signed=False))
    
        return b_id, hk_id, hk, hgc, lgc, crc
        """
        print('Measurement')
        print(b_id)
        print(hk_id)
        print(hk)
        print(hgc)
        print(lgc)
        print(crc)
        """
    def check(self, data, crc):
        crc_val = Crc16CcittFalse.calc(data[0:6])
        #print(crc_val)
        if crc_val == crc:
            return 1
        else:
            return 0
def main():
    GPIO.setmode(GPIO.BCM) 
    GPIO.setwarnings(False)

    #GPIO 5 LP
    GPIO.setup(5, GPIO.OUT) 
    GPIO.output(5, GPIO.LOW) 
    
    for i in range(int(sys.argv[1])):
        measurement = PIP()
        received_data = measurement.measure()
        #print(received_data)
        b_id, hk_id, hk, hgc, lgc, crc = measurement.parse(received_data)
        #print(b_id, hk_id, hk, hgc, lgc, crc)
        crc_v = measurement.check(received_data, crc)
        #print(crc_v)


    #ser.write(received_data)
    GPIO.output(5, GPIO.HIGH)
    GPIO.cleanup()

if __name__ == "__main__":
    main()
