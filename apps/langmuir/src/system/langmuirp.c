#include "app/system/langmuirp.h"

void cmd_langmuirp_init()
{
	cmd_add("lp_get_data", lp_get_data, "%d", 1);
}

int lp_get_data(char *fmt, char *params, int nparams)
{
	lp_data_t data;
	uint32_t lp_msg_len = 128;
	uint32_t time;

	int measurements;
	FILE *lp;
	
	char lp_msg[lp_msg_len];
	
	char path[90] = "python3 apps/"; 
	strcat(path, APP_NAME);
	strcat(path, "/src/system/lp_measurement.py");

	char number[5];
	int rc;

	if(params == NULL)
		return CMD_SYNTAX_ERROR;

	if (sscanf(params, fmt, &measurements)!= nparams) 
		return CMD_SYNTAX_ERROR;

	sprintf(number, " %d", measurements); 
	strcat(path, number);

	time = dat_get_time();

	if (measurements != 0)
		system(path);

	lp = fopen("/home/pi/lp.txt", "r");

	if (lp == NULL)
	{
		LOGI("cmd_mag_get_data", "Null error"); 
		return CMD_ERROR;
	}

	while (fgets(lp_msg, lp_msg_len, lp) != NULL)
	{
		sscanf(lp_msg, "%u %u %u %u %u %u %u", &data.lp_unit,
                                                       &data.lp_hk_idx,
                                                       &data.lp_hk,
                                                       &data.lp_hgch,
                                                       &data.lp_lgch,
                                                       &data.crc,
                                                       &data.chk);

		data.lp_index = dat_get_system_var(dat_drp_idx_lp);
		data.lp_timestamp = time;
		rc = dat_add_payload_sample(&data, lp_sensors);

		if (rc == -1)
			CMD_ERROR;

		time = time + 1;
		}

	fclose(lp);

	return CMD_OK;
}
