import PIP as lp
import RPi.GPIO as GPIO
from time import time
import sys 

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
#GPIO 5 MAG+

GPIO.setup(5, GPIO.OUT)
GPIO.output(5, GPIO.LOW)

lp1 = lp.PIP()

star = time()

open("/home/pi/lp.txt","w").close()
file = open("/home/pi/lp.txt","w")

for i in range(int(sys.argv[1])):
    #print(i)
    data = lp1.measure()
    b_id, hk_id, hk, hgc, lgc, crc = lp1.parse(data)    
    crc_v = lp1.check(data, crc)
    
    file.write("{:d} {:d} {:d} {:d} {:d} {:d} {:d}\n".format(b_id,
                                                             hk_id,
                                                             hk,
                                                             hgc,
                                                             lgc,
                                                             crc,
                                                             crc_v))
#print(star, time.time())
#print(time.time()-star)
GPIO.output(5, GPIO.HIGH)
GPIO.cleanup()

file.close()
